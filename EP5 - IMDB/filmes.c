/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Guerrero
  NUSP: 11275297

  IMDB: filmes.c


  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:
  - função mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/


/*----------------------------------------------------------*/
/* filmes.h e a interface para as funcoes neste modulo      */
#include "filmes.h" 

/*----------------------------------------------------------*/
#include <stdlib.h>  /* NULL, free() */
#include <stdio.h>   /* printf(), scanf() */ 
#include <string.h>  /* strlen(), strncpy(), strcmp(), strtok() */

#include "util.h"    /* Bool, mallocSafe() */
#include "iofilmes.h"
#include "st.h"      /* freeST(), initST(), putFilmeST(), getFilmeST(),
                        showST(), freeST() */

/*Declaração funções auxiliares*/

static Filme *mergesort (Filme* lst, int nFilmes, Criterio criterio);

static Filme *divide (Filme* film, int nFilmes);

static Filme *intercala(Filme *lst, Filme *lst2, Criterio criterio, int nFilmes);

static void quicksort(Filme *cab, Filme *fim, Criterio criterio);

static Filme *separa(Filme *cab, Filme *ult, Criterio criterio);

/*----------------------------------------------------------------------
 *  crieFilme
 *
 *  Recebe informacoes sobre um filme 
 *
 *      - DIST  : distribuicao de notas
 *      - VOTOS : numero de votos
 *      - NOTA  : nota do filme 
 *      - ANO   : ano de producao do filme
 *
 *  e cria uma celula do tipo Filme para armazenar essa informacao. 
 *  A funcao retorna o endereco da celula criada.
 */
Filme *crieFilme (char dist[], int votos, float nota, char *nome, int ano){
    Filme *flm;
    int    len = strlen(nome) + 1; /* +1 para o '\0' */
    
    flm = mallocSafe(sizeof *flm);
    
    strncpy(flm->dist, dist, TAM_DIST+1); /* +1 para o '\0' */
    
    flm->votos = votos;
    flm->nota  = nota;
    
    flm->nome = mallocSafe(len*sizeof(char));
    strncpy(flm->nome, nome, len);
    
    flm->ano  = ano;

    flm->prox = flm->ant = NULL; /* paranoia */
    
    return flm;
}

/*----------------------------------------------------------------------
 *  crieListaFilmes
 * 
 *  Cria uma estrutura que representa lista de filmes vazia.
 *  A funcao retorna o endereco dessa estrutura.
 *
 *  Um lista de filmes e representada por uma lista encadeada 
 *  duplamente ligada com cabeca. 
 */
ListaFilmes *crieListaFilmes(){
    ListaFilmes *ret;
    ret = mallocSafe(sizeof(ListaFilmes));
    ret->nFilmes = 0;
    ret->cab = mallocSafe(sizeof(Filme));
    ret->cab->nota = 99;
    ret->cab->prox = ret->cab->ant = ret->cab;
    return ret;
}

/*----------------------------------------------------------------------
 *  libereListaFilmes(lst)
 *
 *  Recebe um ponteiro lst para uma estrutura que representa uma lista 
 *  de filmes e libera toda a memoria alocada para a lista.
 *
 *  Esta funcao utiliza a funcao libereFilme().
 */

void libereListaFilmes(ListaFilmes *lst){
    Filme* aux;
    aux = lst->cab->prox;
    while (aux != lst->cab){
        aux = aux->prox;
        libereFilme(aux->ant);
    }
    libereFilme(aux);
    free(lst);
}

/*----------------------------------------------------------------------
 *  libereFilme
 *
 *  Recebe um ponteiro FLM para uma estrutura que representa um 
 *  filme e libera a area alocada.
 *
 */
void libereFilme(Filme *flm){
    free(flm->nome);
    free(flm);
}

/*----------------------------------------------------------------------
 *  insiraFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao insere o filme na lista.
 *  
 */
void insiraFilme(ListaFilmes *lst, Filme *flm){
    flm->prox = lst->cab->prox;
    flm->prox->ant = flm;
    lst->cab->prox = flm;
    flm->ant = lst->cab;
    lst->nFilmes++;
}

/*---------------------------------------------------------------------
 *  contemFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao retorna  TRUE se o filme esta na lista e 
 *  FALSE em caso contrario. 
 *
 *  Consideramos que dois filmes f e g sao iguais se
 *
 *       - f->nome e' igual a g->nome 
 *       - f->nota e' igual a g->nota
 *       - f->ano  e' igual a g->ano 
 *
 *  Para comparar dois nomes voce pode usar alguma funcao da 
 *  bibliteca do c  como strcmp, strncmp (string,h) 
 *  ou a funcao strCmp (util.h).
 *
 */
Bool contemFilme(ListaFilmes *lst, Filme *flm){
    Filme* aux;
    aux = lst->cab->prox;
    while (aux != lst->cab){
        if (aux->nota == flm->nota && aux->ano == flm->ano && !strcmp(flm->nome, aux->nome))
            return TRUE;
        aux = aux->prox;
    }
    return FALSE;
}

/*----------------------------------------------------------------------
 *  removaFilme
 *
 *  Remove da lista de filmes LST o filme apontado por FLM.
 *    
 *  Pre-condicao: a funcao supoe que o filme FLM esta 
 *                na lista LST.
 */
void removaFilme(ListaFilmes *lst, Filme *flm){
    flm->ant->prox = flm->prox;
    flm->prox->ant = flm->ant;
    libereFilme(flm);
    lst->nFilmes--;
}

/*----------------------------------------------------------------------
 *  mergeSortFilmes
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo mergeSort recursivo adaptado para listas encadeadas
 *  duplamente ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 *
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */
void mergeSortFilmes(ListaFilmes *lst, Criterio criterio){
    int i;
    Filme *aux;
    lst->cab = mergesort(lst->cab, lst->nFilmes, criterio);
    aux = lst->cab;
    for(i = 0; i < lst->nFilmes; i++)
        aux = aux->prox;
    aux->prox = lst->cab;
    lst->cab->ant = aux;
    if (criterio == NOTA)
        printf("Lista de filmes ordenada por nota\n");
    else
        printf("Lista de filmes ordenada por nome\n");
}


/*----------------------------------------------------------------------
 *  quickSortFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo quickSort adaptado para listas encadeadas duplamente
 *  ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 * 
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */
void quickSortFilmes(ListaFilmes *lst, Criterio criterio){
    Filme *aux;
    quicksort(lst->cab, lst->cab->ant, criterio);
    aux = lst->cab;
    while (aux->prox != lst->cab){
        aux = aux->prox;
    }
    lst->cab->ant = aux;
    if (criterio == NOTA)
        printf("Lista de filmes ordenada por nota\n");
    else
        printf("Lista de filmes ordenada por nome\n");
}

/*----------------------------------------------------------------------
 *  hashFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e distribui as palavras que
 *  ocorrem nos nomes do filmes em uma tabela de dispersao 
 *  (hash table):
 *
 *     http://www.ime.usp.br/~pf/mac0122-2002/aulas/hashing.html
 *     http://www.ime.usp.br/~pf/mac0122-2003/aulas/symb-table.html
 *
 *  Antes de inserir uma palavra na tabela de dispersao todas a
 *  letras da palavra devem ser convertidas para minusculo. Com
 *  isto faremos que a busca de filmes que possuam uma dada
 *  palavra em seu nome nao seja 'case insensitive'. Para essa
 *  tarefa a funcao tolower() pode ser utilizada.
 *
 *  Esta funcao utiliza as funcoes freeST(), initST() e putFilmeST()
 *
 *  Para obter as palavras podemos escrever uma funcao peguePalavra()
 *  inspirada na funcao pegueNome do modulo lexer.c do EP3/EP4 ou
 *  ainda utilizar a funcao strtok() da biblioteca string.h:
 *  
 *      http://linux.die.net/man/3/strtok    (man page)
 *
 */
void
hashFilmes(ListaFilmes *lst)
{
    AVISO(hashFilmes em filmes.c: Vixe ainda nao fiz essa funcao...);
}

/*Funções auxiliares*/

/*
* Recebe uma lista "cab" com cabeça e o número de filmes dessa lista.
* Retorna a lista ordenada.
*/
static Filme *mergesort (Filme* cab, int nFilmes, Criterio criterio){
    Filme *cab2;
    if (nFilmes > 1){
        cab2 = divide(cab, nFilmes);
        cab = mergesort(cab, (nFilmes/2), criterio);
        cab2 = mergesort(cab2, nFilmes - (nFilmes/2), criterio);
        return intercala(cab, cab2, criterio, nFilmes);
    }
    return cab;
}

/*
* Recebe uma lista "cab" com cabeça e o número de filmes dessa lista.
* Retorna uma nova lista com cabeça contendo a metade direita da "cab".
* A função usa mallocsafe() para alocar a cabeça da segunda lista.
*/
static Filme *divide (Filme* cab, int nFilmes){
    int nFilmesAtual;
    Filme *aux, *lst2;
    aux = cab->prox;
    for (nFilmesAtual=0; nFilmesAtual < (nFilmes/2); nFilmesAtual++){
        aux = aux->prox;
    }

    lst2 = mallocSafe(sizeof(Filme));
    cab->ant = NULL;
    lst2->ant = NULL;
    lst2->prox = aux;
    aux->ant->prox = NULL;
    aux->ant = NULL;
    return lst2;
}

/*
* Recebe duas listas "lst" e "lst2" com cabeça ordenadas, o número de filmes e
* o critério de ordenação.
* Retorna uma única lista ordenada contendo o conteúdo de "lst" e "lst2".
* A função usa free() para liberar a cabeça da "lst2".
*/
static Filme *intercala(Filme *lst, Filme *lst2, Criterio criterio, int nFilmes){
    int tamLst2;
    Filme *ret, *fim, *aux;
    tamLst2 = nFilmes - (nFilmes/2); /*Tamanho da metade direita da lista*/
    ret = lst;
    fim = lst;

    lst = lst->prox;
    aux = lst2;
    lst2 = lst2->prox;
    free(aux);

    while (lst != NULL || tamLst2 > 0){
        if (lst != NULL && tamLst2 > 0){
            if (criterio == NOTA){
                if (lst->nota > lst2->nota){
                    aux = lst;
                    lst = lst->prox;
                }
                else{
                    aux = lst2;
                    lst2 = lst2->prox;
                    tamLst2--;
                }
            }
            else if (criterio == NOME){
                if (strCmp(lst->nome, lst2->nome) < 0){
                    aux = lst;
                    lst = lst->prox;
                }
                else{
                    aux = lst2;
                    lst2 = lst2->prox;
                    tamLst2--;
                }
            }
        }
        else if (lst != NULL){
            aux = lst;
            lst = lst->prox;
        }
        else{
            aux = lst2;
            lst2 = lst2->prox;
            tamLst2--;
        }
        fim->prox = aux;
        aux->prox = NULL;
        aux->ant = fim;
        fim = aux;
    }
    return ret;
}

/*
* Recebe uma lista "cab" com cabeça e um ponteiro "fim" pro pivô da lista.
* retorna a lista que vai de cab até fim ordenada.
*/
static void quicksort(Filme *cab, Filme *fim, Criterio criterio){
    Filme *ult;
    ult = separa(cab, fim, criterio);
    if (ult == NULL)
        return;
    quicksort(cab, fim->ant, criterio);
    quicksort(fim, ult, criterio);
}

/* 
* Recebe uma lista "cab" com cabeça e um pivô "ult".
* Retorna uma lista separada pelo pivô.
*/
static Filme *separa(Filme *cab, Filme *ult, Criterio criterio){
    Filme *cab2, *aux, *fimCab1, *fimCab2, *atual;

    if (cab->prox == ult || ult == cab || ult == NULL)
        return NULL;

    cab2 = mallocSafe(sizeof(Filme));
    cab2->prox=NULL;
    fimCab1 = cab;
    fimCab2 = cab2;

    aux = cab->prox;
    while (aux != ult){
        atual = aux;
        aux = aux->prox;
        if (criterio == NOTA){
            if (atual->nota > ult->nota){
                fimCab1->prox = atual;
                atual->ant = fimCab1;
                fimCab1 = atual;
            }
            else{
                fimCab2->prox = atual;
                atual->ant = fimCab2;
                fimCab2 = atual;
            }
        }
        else if(criterio == NOME){
            if (strCmp(atual->nome, ult->nome) < 0){
                fimCab1->prox = atual;
                atual->ant = fimCab1;
                fimCab1 = atual;
            }
            else{
                fimCab2->prox = atual;
                atual->ant = fimCab2;
                fimCab2 = atual;
            }
        }
    }
    fimCab1->prox = ult;
    ult->ant = fimCab1;
    fimCab2->prox = ult->prox;
    ult->prox = cab2->prox;
    cab2->prox->ant = ult;
    free(cab2);  
    return fimCab2;
}