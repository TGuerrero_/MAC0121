/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Guerrero
  NUSP: 11275297

  stack.c
  Pitao I

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.

  Exemplo:
  - função mallocc retirada de: 
 
    http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* interface para o uso de uma pilha */
#include "stack.h" 
#include "util.h"
#include <stdlib.h>

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */

Stack stackInit(){
  Stack pilha;
  pilha = mallocSafe(sizeof(*pilha));
  pilha->prox = NULL;
  return pilha;
}

int stackEmpty(Stack pilha){
  return pilha->prox == NULL;
}

void stackPush(Stack pilha, Item item){
  Stack aux;

  aux = mallocSafe (sizeof(*aux));
  aux->conteudo = item;
  aux->prox = pilha->prox;
  pilha->prox = aux;
}

Item stackPop(Stack pilha){
  Item pop;
  Stack aux;

  aux = pilha->prox;
  pop = aux->conteudo;
  pilha->prox = aux->prox;
  free(aux);

  return pop;
}

void stackFree(Stack pilha){
  Stack aux;
  while (pilha->prox != NULL){
    aux = pilha->prox;
    pilha->prox = aux->prox;
    free(aux);
  }
  free(pilha);
}

void stackDump(Stack pilha){
  Stack aux;

  aux = pilha->prox;
  if (aux == NULL)
    printf ("\nPilha Vazia!");

  else
    while (aux != NULL){
      printf ("\n %f", aux->conteudo.valor.vFloat);
      aux = aux->prox;
    }
  printf ("\n");
}