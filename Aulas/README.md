Leitura recomendada
=

[Projeto de Algoritmos (em C)](https://www.ime.usp.br/~pf/algoritmos/)


Programação das aulas
=

**⋅** **1 de agosto (Aula 1):**

    -Apresentação da disciplina
    -Recursão: Hanoi e fatorial

**⋅** **6 de agosto (Aula 2):**

    -Recursão: cálculo do máximo de um vetor e binomial
    -Quando recursão não é recomendada?

**⋅** **8 de agosto (Aula 3):**

    -Mais recursão: Fibonacci, Hanoi, mdc
    -echo

**⋅** **13 de agosto (Aula 4):**

    -Mais recursão: mdc e curvas de Hilbert

**⋅** **15 de agosto (Aula 5):**

    -Registros e estruturas
    -Endereços e ponteiros
    -Alocação dinâmica

**⋅** **20 de agosto (Aula 6):**

    -Alocação dinâmica
    -Listas em vetores
    -Listas encadeadas

**⋅** **22 de agosto (Aula 7):**

    -Listas encadeadas com ponteiros

**⋅** **27 de agosto (Aula 8):**

    -Listas encadeadas com ponteiros
    -Listas encadeadas com cabeça
    -Inversão de lista encadeada

**⋅** **29 de agosto (Aula 9):**

    -Pilhas
    -Pilha de execução
    -Notação posfixa e cálculo de expressões
    
**⋅** **10 de setembro (Aula 10):**

    -Notação polonesa
    -Interfaces, clientes e tipos abstratos de dados

**⋅** **12 de setembro (Aula 11):**

    -Interfaces
    -Um cliente e duas implementações da interface de Pilha
    -Implementações da notação polonesa com as diferentes pilhas

**⋅** **17 de setembro (Aula 12):**

    -Pilha com lista encadeada sem cabeça
    -Filas e distâncias

**⋅** **19 de setembro (Aula 13):**

    -Implementações de filas
    -Fila em vetor e vetor circular

**⋅** **24 de setembro (Aula 14):**

    -Dúvidas sobre as listas
    -Fila com lista encadeada
    
**⋅** **1 de outubro (Aula 15):**

    -Mais duas implementações de fila com lista encadeada
    -Implementações da distancia com as diferentes filas

**⋅** **3 de outubro (Aula 16):**

    -Busca sequencial e binária
    -Ordenação por inserção
    -Ordenação por seleção

**⋅** **15 de outubro (Aula 17):**

    -Experimentos com os algoritmos de ordenação anteriores
    -Intercalação
    -Mergesort

**⋅** **17 de outubro (Aula 18):**

    -Quicksort

**⋅** **22 de outubro (Aula 19):**

    -Seleção do k-ésimo
    -Filas de prioridade e heaps
    -Heapsort

**⋅** **24 de outubro (Aula 20):**

    -Complemento da análise e comentários sobre heaps e o heapsort
    -Problema do k-ésimo menor elemento
    -Árvores binárias

**⋅** **29 de outubro (Aula 21):**

    -Percursos em árvores
    -Construção de uma árvore a partir de expressões com algoritmo de posfixa!

**⋅** **31 de outubro (Aula 22):**
    
    -Tabelas de símbolos
    -Árvores de busca binária
    
**⋅** **12 de novembro (Aula 23):**

    -Árvores de busca binária
    -Treaps

**⋅** **14 de novembro (Aula 24):**

    -Hashing, tabelas de símbolos

**⋅** **19 de novembro (Aula 25):**

    -Hashing: sondagem linear
    -Busca de padrão
    -Boyer-Moore

**⋅** **21 de novembro (Aula 26):**

    -Segunda estratégia de Boyer-Moore
    -Backtracking
    -Problema das n rainhas

**⋅** **26 de novembro (Aula 27):**

    -Problema dos cavalos
    -Enumeração
    