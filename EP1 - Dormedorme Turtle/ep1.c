/*\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Guerrero
  NUSP: 11275297

  ep1.c

  Referências: 
  

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__*/

#include <stdio.h>  /* scanf(), printf(), ... */
#include <stdlib.h> /* srand(), rand(), atoi(), exit(), ...  */
#include <string.h> /* strlen(), strcmp(), ... */  

/*---------------------------------------------------------------*/
/* 
 * 0. C O N S T A N T E S 
 */

/* tamanho máximo de um turtledorm */
#define MAX      128

/* estado da turtle */
#define ACORDADO   '#'
#define DORMINDO   ' '
#define TAPINHA    'T'

#define TRUE       1
#define FALSE      0

#define ENTER      '\n'
#define FIM        '\0'
#define ESPACO     ' '

/*---------------------------------------------------------------*/
/*
 * 1. P R O T Ó T I P O S
 */

/* PARTE I */
void
leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]);

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE II */
void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);


/* PARTE III */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* FUNÇÕES AUXILIARES */

int randomInteger(int low, int high);

void  pause();

void incremente(int bin[]);

void copy (int A[][MAX], int tDorm[][MAX], int nLin, int nCol);


/*---------------------------------------------------------------*/
/* 
 *  M A I N 
 */
int main(int argc, char *argv[]){
    char in, play[4];
    int nLin, nCol, tDorm[MAX][MAX];
    int cont, lin, col, check;
    
    printf ("\nLight Out\n");
    printf ("------------------------------------------------------------\n");
    printf ("Digite 's' para (s)ortear ou\n");
    printf ("       'l' para (l)er um turtledorm de arquivo.\n");
    printf (">>> ");
    scanf ("%c", &in);


    if (in == 'l')
        leiaTurtledorm(&nLin, &nCol, tDorm);

    else if (in == 's')
        sorteieTurtledorm(&nLin, &nCol, tDorm);
    
    else{
        printf ("Opção inválida.\n");
        pause();
    }

    printf ("\nTurtledorm inicial\n");
    mostreTurtledorm (nLin, nCol, tDorm, ACORDADO);
    printf ("\nUm tapinha é definido por dois inteiros lin e col, onde 1 <= lin <= 5 e 1 <= col <= 5\n");
    printf ("\n------------------------------------------------------------\n");

    for (cont =0, check=1; todosDormindo (nLin, nCol, tDorm) != TRUE && check == 1;){
        printf ("Digite 'd' para desistir,\n");
        printf ("       'a' para receber (a)juda para encontrar uma solucao,\n");
        printf ("       'g' para (g)ravar o turtledorm atual, ou\n");
        printf ("       'lin col' para dar um tapinha na posicao [lin][col].\n");
        printf (">>> ");
        scanf (" %s", play);

        if (play[0] == 'd')
            check = 0;

        else if (play[0] == 'g')
            graveTurtledorm (nLin, nCol, tDorm);

        else if (play[0] == 'a')
            resolvaTurtledorm(nLin, nCol, tDorm);
            
        else{
            cont++;
            lin = atoi(play);
            scanf ("%d", &col);
            tapinhaTurtle(nLin, nCol, tDorm, lin, col);
            printf ("\nTurtledorm após %d tapinhas.", cont);
            mostreTurtledorm (nLin, nCol, tDorm, ACORDADO);
        }
    }

    if (check){
    printf ("Parabens, voce colocou todos para dormir apos %d tapinha(s)!\n", cont);
    printf ("Todos ja estao dormindo. Nao faca barulho!\n");
    }
    else{
        printf ("Desta vez nao deu.\n");
        printf ("Voce deu %d tapinha(s).\n", cont);
        printf ("Melhor sorte na proxima!\n");
    }

    pause();
    return EXIT_SUCCESS;
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                        P A R T E   I 
 */

void leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]){
    FILE * save;
    char input[MAX];
    int i, j;

    printf ("Digite o nome do arquivo de onde carregar o turtledorm: ");
    scanf ("%s", input);
    save = fopen(input, "r");
    
    fscanf (save, "%d %d", &(*nLin), &(*nCol));
    
    for (i=0; i < *nLin; i++)
        for (j=0; j < *nCol; j++)
            fscanf (save, "%d", &tDorm[i][j]);

    fclose(save);
}

void  mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c){
    int i, j, k;

    printf ("\n");
    printf ("   ");
    for (i=0; i < nCol; i++)
        printf("   %d  ", i+1);
    printf ("\n");

    printf ("    ");
    for (i=0; i < nCol; i++)
        printf("+-----");
    printf ("+\n");

    for (i=0; i < nLin; i++){
        printf (" %2d |", i+1);
        for (j=0; j < nCol; j++){
            if (tDorm [i][j] == 0)
                printf ("     ");
            else if (c == ACORDADO)
                printf ("  #  ");
            else if (c == TAPINHA)
                printf ("  T  ");
            printf ("|");
        }
        printf ("\n");
        printf ("    ");
            for (k=0; k < nCol; k++)
                printf("+-----");
        printf ("+\n");
    }
}

void tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col){
    int jLin, jCol;
    jLin = lin-1; /*Muda o intervalo da jogada de [1, n] para [0, n-1].*/
    jCol = col-1;

    tDorm[jLin][jCol] = !tDorm[jLin][jCol];

    if (jLin-1 >= 0)
        tDorm[jLin-1][jCol] = !tDorm[jLin-1][jCol]; 
    if (jLin+1 < nLin)
        tDorm[jLin+1][jCol] = !tDorm[jLin+1][jCol];

    if (jCol-1 >= 0)
        tDorm[jLin][jCol-1] = !tDorm[jLin][jCol-1];
    if (jCol+1 < nCol)
        tDorm[jLin][jCol+1] = !tDorm[jLin][jCol+1];
}

int todosDormindo(int nLin, int nCol, int tDorm[][MAX]){
    int i, j, check=1;

    for (i=0; i < nLin && check == 1; i++)
        for (j=0; j < nCol && check == 1; j++)
            if (tDorm[i][j] == 1)
                check = 0;
    if (check)
        return TRUE;
    else
        return FALSE;
}

int graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]){
    FILE * save;
    char name[MAX];
    int i, j;

    printf ("Digite o nome do arquivo onde salvar o turtledorm: ");
    scanf ("%s", name);
    save = fopen (name, "w");

    if (save == NULL)
        return EXIT_FAILURE;

    fprintf (save, "%d", nLin);
    fprintf (save, " ");
    fprintf (save, "%d", nCol);
    fprintf (save, "\n");

    for (i=0; i < nLin; i++){
        for (j=0; j < nCol; j++){
            fprintf (save, "%d", tDorm[i][j]);
            fprintf (save, " ");
        }
        fprintf (save, "\n");
    }
    fclose(save);
    printf ("Turtledorm foi salvo no arquivo '%s'\n", name);
    return EXIT_SUCCESS;
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                        P A R T E   II  
 */

void sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]){
    int i, j, seed, nTapinhas, turtles=0;
    char dificuldade;

    printf ("Digite a dimensao do turtledorm (nLin nCol): ");
    scanf ("%d %d", nLin, nCol);
    printf ("Digite um inteiro para o gerador de numeros aleatorio (semente): ");
    scanf ("%d", &seed);
    printf ("Digite o nivel de dificuldade [f/m/d]: ");
    scanf (" %c", &dificuldade);

    for (i=0; i < (*nLin); i++)
        for (j=0; j < (*nCol); j++)
            tDorm[i][j] = 0;
    
    if (dificuldade == 'f')
        nTapinhas = randomInteger( (0.05*(*nLin)*(*nCol)), (0.2*(*nLin)*(*nCol)) );

    else if (dificuldade == 'm')
        nTapinhas = randomInteger( (0.25*(*nLin)*(*nCol)), (0.5*(*nLin)*(*nCol)) );

    else 
        nTapinhas = randomInteger( (0.55*(*nLin)*(*nCol)), (0.85*(*nLin)*(*nCol)) );

    srand(seed);
    for (i=nTapinhas; i > 0; i--)
        tapinhaTurtle((*nLin), (*nCol), tDorm, rand()%(*nLin), rand()%(*nCol));
        
    for (i=0; i < (*nLin); i++)
        for (j=0; j < (*nCol); j++)
            if (tDorm[i][j] == 1)
                turtles++;

    printf ("\nNumero de tapinhas sorteado = %d.\n", nTapinhas);
    printf ("Numero de turtles despertos = %d.\n", turtles);
}

/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                        P A R T E   III 
 */

void resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]){
    int tapinhas[MAX], bin[MAX+1];
    int sol[MAX][MAX], copia[MAX][MAX];
    int i, j, tapas=0, tapasl, check=1;

    /*
    tapinhas = Vetor que codifica a solução atual;
    sol = Matriz que guarda a melhor solução;
    bin = Matriz binária que codifica a solução da primeira linha do tabuleiro;
    copia = Matriz que faz uma cópia do estado atual do tabuleiro do jogo;
    tapas = Quantidade de tapas da melhor solução;
    tapasl = Quantidade de tapas da solução atual.
    */

    for (j=0; j < nCol+1; j++){
        bin[j] = 0;
        for (i=0; i < nLin; i++){
            if (j < nCol)
                sol[i][j] = 0;
        }
    }

    while (bin[nCol] == 0){ /*Testa todos os possíveis tapas na primeira linha e tenta resolver*/
        tapasl = 0;            /*pelo método do "light chassing" buscando a melhor solução.*/
        copy(copia, tDorm, nLin, nCol);

        for (i =0; i < nLin*nCol; i++)
            tapinhas[i] = 0;

        for (i=0; i < nCol; i++) /*Testa os tapas do vetor binário da vez na primeira linha*/
            if (bin[i] == 1){
                tapinhaTurtle(nLin, nCol, copia, 1, i+1);
                tapinhas[i] = 1;
                tapasl++;
            }

        for (i=0; i < nLin; i++)
            for (j=0; j < nCol; j++)
                if (i+1 < nLin && copia[i][j] == 1){
                    tapinhaTurtle(nLin, nCol, copia, i+2, j+1);
                    tapinhas[(i+1)*nCol + j] = 1;
                    tapasl++;
                }

        if ((todosDormindo(nLin, nCol, copia) == TRUE && tapasl < tapas) || (todosDormindo(nLin, nCol, copia) == TRUE && check == 1)){
        /*Entra se for a primeira solução encontrada, se não, entra se a quantidade
         de tapas da nova solução encontrada for menor do que a da atual*/
            check = 0;
            tapas = tapasl;
            for (i=0; i < nLin; i++)
                for (j=0; j < nCol; j++)
                    sol[i][j] = tapinhas[i*nCol + j];
        }

        incremente(bin);  
    }

    if (tapas == 0)
       printf ("\nNao tem solução!\n");

    else{
        printf ("\nSOLUCAO MENOS VIOLENTA\n");
        printf ("O menor numero de tapinhas de uma solução é %d\n", tapas);
        mostreTurtledorm(nLin, nCol, sol, TAPINHA); 
    }
}
 
/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                     A U X I L I A R E S 
 */

/* 
 * incremente(bin)
 * 
 * Recebe através do vetor BIN a representação de um 
 * número binário k e devolve em BIN a representação 
 * binária de k+1.
 * 
 * Pré-condição: a função não se preocupa com overflow,
 *   ou seja, supõe que k+1 pode ser representado em 
 *   BIN.
 */ 
void incremente(int bin[]){
    int i;

    for (i = 0; bin[i] != 0; i++)
        bin[i] = 0;

    bin[i] = 1;
}

/*
* copy()
*
* A função recebe uma matriz A e uma matriz tDorm com nLin x nCol
* e copia o conteúdo de tDorm para a matriz A.
*/
void copy (int A[][MAX], int tDorm[][MAX], int nLin, int nCol){
    int i, j;

    for (i=0; i < nLin; i++)
        for (j=0; j < nCol; j++)
            A[i][j] = tDorm[i][j];
}

/* 
 * randomInteger()
 *
 * A função recebe dois inteiros LOW e HIGH e retorna um 
 * inteiro aleatório entre LOW e HIGH inclusive, ou seja, 
 * no intervalo fechado LOW..HIGH.
 *
 * Pré-condição: a função supõe que 0 <= LOW <= HIGH < INT_MAX.
 *     O codigo foi copiado da página de projeto de algoritmos 
 *     de Paulo Feofiloff, que por sua vez diz ter copiado o 
 *     código da biblioteca random de Eric Roberts.
 */
int randomInteger(int low, int high){
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}

/* 
 * pause()
 *
 * Para a execução do programa até que um ENTER seja digitado.
 */
void pause(){
    char ch;

    printf("Digite ENTER para continuar. ");
    do 
    {
        scanf(" %c", &ch);
    }
    while (ch != ENTER);
}
