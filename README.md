# MAC0121 - Estrutura de Dados I

Repositório dedicado para a disciplina MAC0121 - Estrutura de Dados I - ministrada no segundo semestre de 2019.

Professora: Cristina Gomes Fernandes

Site da disciplina: https://www.ime.usp.br/~cris/mac121

# Material didático

Todo material disponibilizado para aprendizado (aulas e listas) foram disponibilizados no decorrer do semestre, portanto, são de posse da USP.