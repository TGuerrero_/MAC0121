/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Guerrero
  NUSP: 11275297

  posfixa.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função mallocc retirada de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* iterface para o uso da funcao deste módulo */
#include "posfixa.h"

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES, INDEFINIDA, 
                           ABRE_PARENTESES, ... */
#include "objetos.h" /* tipo CelObjeto, freeObjeto(), freeListaObjetos() */
#include "stack.h"   /* stackInit(), stackFree(), stackPop() 
                        stackPush(), stackTop() */


/*Funções auxiliares*/
static CelObjeto* QueueNew (CelObjeto *atual, CelObjeto *fim);

/*-------------------------------------------------------------
 *  infixaParaPosfixa
 * 
 *  Recebe uma lista ligada com cabeca INIINFIXA representando uma
 *  fila de itens de uma expressao infixa e RETORNA uma lista ligada
 *  com cabeca contendo a fila com a representacao da correspondente
 *  expressao em notacao posfixa.
 */
 /*  As celulas da notacao infixa que nao forem utilizadas na notacao 
  *  posfixa (abre e fecha parenteses) devem ser liberadas 
  *  (freeObjeto()).
  */
CelObjeto * infixaParaPosfixa(CelObjeto *iniInfixa){
  CelObjeto *aux, *fim, *atual, *pop;
  Stack pilha;

  pilha = stackInit();
  fim = iniInfixa; /*Equivalente a dizer fim = ini*/

  aux = iniInfixa->prox;
  while (aux != NULL){
    atual = aux;
    aux = aux->prox;
    switch (atual->categoria){
      case FLOAT:
      case ID:
        fim = QueueNew(atual, fim);
        break;

      case ABRE_PARENTESES:
        stackPush(pilha, atual);
        break;

      case FECHA_PARENTESES:
        if (!stackEmpty(pilha)){
          pop = stackPop(pilha);
          while (pop->categoria != ABRE_PARENTESES && !stackEmpty(pilha)){
            fim = QueueNew(pop, fim);
            pop = stackPop(pilha);
          }
        }
        freeObjeto(pop);
        freeObjeto(atual);
        break;

      default:
        if (atual->categoria <= 18){
          while (!stackEmpty(pilha) && stackTop(pilha)->categoria <= 18 && stackTop(pilha)->prec >= atual->prec){
            if ((stackTop(pilha)->prec == 8 || stackTop(pilha)->prec == 1) && stackTop(pilha)->prec == atual->prec)
              break;
            pop = stackPop(pilha);
            fim = QueueNew(pop, fim);
          }
          stackPush(pilha, atual);
        }
        else{
          stackPush(pilha, atual);
        }
        break;
    };
  }
  
  while (!stackEmpty(pilha)){
    pop = stackPop(pilha);
    fim = QueueNew(pop, fim);
  }

  stackFree(pilha);
  return iniInfixa; 
}

/*Funções auxiliares*/
/*
* QueueNeW();
* Recebe um CelObjeto e coloca ele no fim da fila.
*/
static CelObjeto* QueueNew (CelObjeto *atual, CelObjeto *fim){
  atual->prox = NULL;
  fim->prox = atual;
  fim = atual;
  return fim;
}