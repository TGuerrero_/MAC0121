#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 0

typedef struct stack Pilha;
struct stack {
    char valor;
    Pilha *prox;
};

typedef struct queueNode Fila;
struct queueNode {
    int valor;
    Fila *prox;
};

void *mallocSafe(size_t n){
    void * p;

    p = malloc(n);
    if (p == NULL) 
    {
        printf("ERRO na alocacao de memoria.\n\n");
        exit(EXIT_FAILURE);
    }

    return p;
}


/*1)Bem formada*/
int bemFormada(int n, char *string){
    int flag = TRUE;
    Pilha *pilha = NULL, *novo;
    for (int i=0; flag && i < n; i++){
        switch (string[i]){
        case ')':
            if (pilha == NULL || pilha->valor != '(')
                flag = FALSE;
            else{
                Pilha *lixo = pilha;
                pilha = pilha->prox;
                free(lixo);
            }
            break;
        case ']':
            if (pilha == NULL || pilha->valor != '[')
                flag = FALSE;
            else{
                Pilha *lixo = pilha;
                pilha = pilha->prox;
                free(lixo);
            }
            break;
        case '{':
            if (pilha == NULL || pilha->valor != '}')
                flag = FALSE;
            else{
                Pilha *lixo = pilha;
                pilha = pilha->prox;
                free(lixo);
            }
            break;
        default:
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        }
    }
    return flag;
}

/*2)Posfixa*/
char* posfixa(char *string){
    Pilha *pilha = NULL, *novo;
    int n = strlen(string), j=0;
    char* posfixa = mallocSafe((n+1) * sizeof(char));
    for (int i=0; i < n; i++){
        switch (string[i]){
        case '(':
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        case ')':
            while (pilha != NULL && pilha->valor != '('){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            Pilha *lixo = pilha;
            pilha = pilha->prox;
            free(lixo);
            break;
        case '+':
        case '-':
            while (pilha != NULL && pilha->valor != '('){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        case '*':
        case '/':
            while(pilha != NULL && pilha->valor != '(' && pilha->valor != '+' && pilha->valor != '-'){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        default:
            posfixa[j] = string[i];
            j++;
            break;
        }
    }
    while (pilha != NULL){
        Pilha *lixo = pilha;
        posfixa[j] = pilha->valor;
        j++;
        pilha = pilha->prox;
        free(lixo);
    }
    posfixa[n] = '\0';
    return posfixa;
}

/*3)Posfixa com operador ^*/
char* posfixaExtendida(char *string){
    Pilha *pilha = NULL, *novo;
    int n = strlen(string), j=0;
    char* posfixa = mallocSafe((n+1) * sizeof(char));
    for (int i=0; i < n; i++){
        switch (string[i]){
        case '(':
        case '^':
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        case ')':
            while (pilha != NULL && pilha->valor != '('){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            Pilha *lixo = pilha;
            pilha = pilha->prox;
            free(lixo);
            break;
        case '+':
        case '-':
            while (pilha != NULL && pilha->valor != '('){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        case '*':
        case '/':
            while(pilha != NULL && pilha->valor != '(' && pilha->valor != '+' && pilha->valor != '-'){
                //pop
                Pilha *lixo = pilha;
                posfixa[j] = pilha->valor;
                j++;
                pilha = pilha->prox;
                free(lixo);
            }
            //push
            novo = mallocSafe(sizeof(Pilha));
            novo->valor = string[i];
            novo->prox = pilha;
            pilha = novo;
            break;
        default:
            posfixa[j] = string[i];
            j++;
            break;
        }
    }
    while (pilha != NULL){
        Pilha *lixo = pilha;
        posfixa[j] = pilha->valor;
        j++;
        pilha = pilha->prox;
        free(lixo);
    }
    posfixa[n] = '\0';
    return posfixa;
}

/*4)Distância(bfs)*/
/*
* n = Número de vértices
* c = cidade inicial
*/
void distancia(int n, int **grafo, int c){
    int *dist = mallocSafe(n * sizeof(int));
    Fila*ini, *fim, *novo;
    novo = mallocSafe(sizeof(Fila));
    novo->valor = c;
    novo->prox = NULL;
    ini = fim = novo;

    for (int i=0; i < n; i++){
        dist[i] = n;
    }
    dist[c] = 0;
    while (ini != NULL){
        Fila *atual = ini;
        ini = ini->prox;
        if (atual == fim)
            fim = NULL;
        for (int j=0; j < n; j++){
            if (grafo[atual->valor][j] && dist[j] > dist[atual->valor]+1){
                dist[j] = dist[atual->valor]+1;
                novo = mallocSafe(sizeof(Fila));
                novo->valor = j;
                novo->prox = NULL;
                if (fim != NULL)
                    fim->prox = novo;
                else
                    ini = novo;
                fim = novo;
            }
        }
        free(atual);
    }

    for(int i=0; i < n; i++){
        if (dist[i] < n)
            printf("\nÉ possível chegar na cidade %d", i);
        else
            printf("\nNão é possível chegar na cidade %d", i);
    }
    free(dist);
}

/*5)Bipartido*/
int bipartido(int n, int**grafo){
    int flag = TRUE;
    int *cor = mallocSafe(n * sizeof(int));
    Fila *ini, *fim, *novo;
    novo = mallocSafe(sizeof(Fila));
    novo->valor = 0;
    novo->prox = NULL;
    ini = fim = novo;

    for (int i=0; i < n; i++){
        cor[i] = -1;
    }

    while (ini != NULL){
        Fila*atual = ini;
        ini = ini->prox;
        if (fim == atual)
            fim = NULL;
        for (int j=0; j < n; j++){
            if (grafo[atual->valor][j]){
                if (cor[j] == -1){
                    cor[j] = !cor[atual->valor];
                    novo = mallocSafe(sizeof(Fila));
                    novo->valor = j;
                    novo->prox = NULL;
                    if (fim != NULL)
                        fim->prox = novo;
                    else
                        ini = novo;
                    fim = novo;
                }
                else if (cor[j] == cor[atual->valor])
                    flag = FALSE;
            }
        }
        free(atual);
    }
    free(cor);
    return flag;
}

/*8)Ratinho*/
/*
0 - Parede
1- Caminho livre
*/
int ratinho(int**labirinto, int n, int posx, int posy, int queijox, int queijoy){
    int **dist = mallocSafe(n * sizeof(int*));
    int *fila = mallocSafe (n*n * sizeof(int));
    int ini=0, fim=0;

    for (int i=0; i < n; i++){
        dist[i] = mallocSafe(n * sizeof(int));
    }

    for (int i=0; i < n; i++){
        for (int j=0; j < n; j++)
            dist[i][j] = -1;
    }

    dist[posx][posy] = 0;

    return 0;
}

int main (){
    /*2)*/
    printf("\n%s", posfixa("A-B/C*(D-(E+F))/G+H*I"));
    printf("\n%s", posfixa("(A+B)/C*D-E/(F*G)"));
    printf("\n");

    /*3)*/
    printf("\n%s", posfixaExtendida("A^B^C+D"));
    printf("\n%s", posfixaExtendida("A^B+C^D"));
    printf("\n%s", posfixaExtendida("A^B+C*D^E^F-G/H^(I+J)"));
    printf("\n");
}


