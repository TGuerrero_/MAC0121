#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 0

struct cel {
    int valor;
    struct cel *prox;
};

typedef struct cel Celula;

void *mallocSafe(size_t n){
    void * p;

    p = malloc(n);
    if (p == NULL) 
    {
        printf("ERRO na alocacao de memoria.\n\n");
        exit(EXIT_FAILURE);
    }

    return p;
}


/*1)Recebe uma lista encadeada ini de números inteiros e devolve a soma dos números na lista.
Suponha que a lista encadeada não tem cabeça de lista.*/
int soma (Celula *ini){
    int sum = 0;
    Celula *aux = ini;
    while (aux != NULL){
        sum += aux->valor;
        aux = aux->prox;
    }
    return sum;
}

int somaR (Celula *ini){
    int sum = 0;
    if (ini->prox != NULL)
        sum = soma(ini->prox);
    return (sum+ini->valor);
}

/*2)Recebe uma lista encadeada ini de números inteiros e um inteiro x, devolve o número de vezes
que x aparece na lista. Suponha que a lista encadeada não tem cabeça de lista.*/
int conta (Celula *p, int x){
    int cont=0;
    Celula *aux=p;
    while (aux != NULL){
        if (aux->valor == x)
            cont++;
        aux = aux->prox;
    }
    return cont;
}

int contaR (Celula*p, int x){
    int cont=0;
    if (p->prox != NULL)
        cont = contaR(p->prox, x);
    if (p->valor == x)
        return (cont+1);
    return (cont); 
}

/*3)Recebe uma lista encadeada ini de números inteiros e devolve TRUE se a lista está em ordem
crescente e FALSE caso contrário. Suponha que a lista encadeada não tem cabeça de lista.*/
int crescente (Celula *ini){
    int flag = TRUE;
    Celula *aux = ini;
    while (flag && aux->prox != NULL){
        if (aux->valor > aux->prox->valor)
            flag = FALSE;
        aux = aux->prox;
    }
    return flag;
}

/*4)Recebe uma lista encadeada ini de números inteiros e soma delta no conteúdo de cada elemento
da lista, removendo elementos cujo resultado da soma seja nulo. Suponha que a lista encadeada tem
cabeça de lista.*/
void acrescenta (Celula *ini, int delta){
    Celula *aux = ini, *aux2 = ini->prox, *lixo;
    while (aux2 != NULL){
        aux2->valor += delta;
        if (aux2->valor == 0){
            aux->prox = aux2->prox;
            lixo = aux2;
            aux2 = aux2->prox;
            free(lixo);
        }
        else{
            aux = aux2;
            aux2 = aux2->prox;
        }
    }
}

/*5)Remove todas as cópias de x da lista encadeada apontada por ini . Suponha que a lista tem cabeça
de lista.*/
void remove_todos(Celula *ini, int x){
    Celula *aux = ini, *aux2=ini->prox, *lixo;
    while (aux2 != NULL){
        if (aux2->valor == x){
            aux->prox = aux2->prox;
            lixo = aux2;
            aux2 = aux2->prox;
            free(lixo);
        }
        else
            aux = aux2;
    }
}

/*6)Remove todos os elementos repetidos da lista encadeada ini , deixando apenas uma cópia de cada
um. Suponha que a lista tem cabeça. Utilize a função do exercício anterior mesmo que você não a
tenha feito.*/
void tira_repeticao(Celula *ini){
    Celula *aux = ini;
    while (aux != NULL){
        remove_todos(aux, aux->prox->valor);
        aux = aux->prox;
    }
}

/*7)Um conjunto de números inteiros pode ser armazenado em uma lista encadeada. Por representarem
conjuntos, tais listas não têm elementos repetidos.

Faça uma função que recebe duas listas encadeadas ini1 e ini2 , sem cabeça de lista, cada uma contendo um conjunto
de números inteiros, e devolve uma nova lista encadeada sem cabeça, com a interseção dos dois conjuntos. Você não 
deve destruir as listas dadas. Você deve alocar novas células para compor a lista resultante.*/
Celula *interseccao(Celula *ini1, Celula *ini2){
    Celula *inter = NULL, *aux1 = ini1, *aux2;
    while (aux1 != NULL){
        aux2 = ini2;
        while (aux2 != NULL && aux2->valor != aux1->valor){
            aux2 = aux2->prox;
        }
        if (aux2 != NULL){
            Celula *novo = mallocSafe(sizeof(Celula));
            novo->valor = aux1->valor;
            novo->prox = inter;
            inter = novo;
        }
        aux1 = aux1->prox;
    }
    return inter;
}

/*
Faça uma função que recebe duas listas encadeadas ini1 e ini2 , sem cabeça de lista, cada uma contendo um conjunto
de números inteiros, e devolve uma nova lista encadeada sem cabeça, com a união dos dois conjuntos. (Não se esqueça
que sua lista devolvida não deve ter repetições, para representar corretamente um conjunto.) Você não deve destruir
as listas dadas. Você deve alocar novas células para compor a lista resultante.
*/
Celula *uniao(Celula *ini1, Celula *ini2){
    Celula *uni = NULL, *aux, *novo;
    aux = ini1;
    while (aux != NULL){
        novo = mallocSafe(sizeof(Celula));
        novo->prox = uni;
        novo->valor = aux->valor;
        uni = novo;
        aux = aux->prox;
    }
    aux = ini2;
    while (aux != NULL){
        novo = mallocSafe(sizeof(Celula));
        novo->prox = uni;
        novo->valor = aux->valor;
        uni = novo;
        aux = aux->prox;
    }
    tira_repeticao(uni);
    return uni;
}

/*9)Recebe uma lista encadeada ini com inteiros e rearranja essa lista de forma que ela fique em ordem
crescente, devolvendo o início da lista resultante. Suponha que a lista tem cabeça. Não troque o
conteúdo das células. Apenas altere, quando necessário, os apontadores para ordenar a lista (imagine
que as informações guardadas em cada célula da lista são muitas e que seria muuuuito caro trocar o
conteúdo de duas células).*/
void ordena(Celula *ini){ 
    Celula *novaCabeca = mallocSafe(sizeof(Celula));
    novaCabeca->prox = NULL;
    Celula *aux= ini;
    while (aux->prox != NULL){
        Celula *aux2 = aux;
        Celula *predMax = aux2;
        Celula *max;
        while (aux2->prox != NULL){
            if (predMax->prox->valor < aux2->prox->valor)
                predMax = aux2;
            aux2 = aux2->prox;
        }
        max = predMax->prox;
        predMax->prox = max->prox;
        max->prox = novaCabeca->prox;
        novaCabeca->prox = max;
    }
    ini->prox = novaCabeca->prox;
    free(novaCabeca);
}

/*Auxiliares*/
void insere (Celula*ini, int x){
    Celula *novo = mallocSafe(sizeof(Celula));
    novo->valor = x;
    novo->prox = ini->prox;
    ini->prox = novo;
}

void print (Celula *ini){
    Celula *aux = ini->prox;
    while (aux != NULL){
        printf("%d ", aux->valor);
        aux = aux->prox;
    }
}

int main(){
    //Testes
    Celula *ini = mallocSafe(sizeof(Celula));
    insere(ini, 1);
    insere(ini, 2);
    insere(ini, 3);
    insere(ini, 4);
    insere(ini, 5);
    print (ini);
    ordena(ini);
    printf("\n");
    print (ini);
    return 0;
}